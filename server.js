if (process.env.NODE_ENV !== "production") {
    require("dotenv").config()
}
const express = require("express")
const expressLayouts = require("express-ejs-layouts")
const morgan = require("morgan")
const passport = require('passport')
const methodOverride = require('method-override')
const session = require("express-session")
const MongoStore = require('connect-mongo')
const connectDB = require("./db")

//Passport config
require('./passport')(passport)

connectDB()

const app = express()

//Body parser
app.use(express.urlencoded({ extended: false }))
app.use(express.json())

//Logging
if (process.env.NODE_ENV === 'development') {
    app.use(morgan('dev'))
}

//Handlebars
app.set("view engine", "ejs")
app.set("views", __dirname + "/views")
app.set("layout", "layouts/layout")
app.use(expressLayouts)
app.use(methodOverride('_method'))

//Session
app.use(session({
    secret: 'keyboard cat',
    resave: false,
    saveUninitialized: false,
    store: MongoStore.create({
        mongoUrl: process.env.MONGO_URI
    })
}))

//Passport middleware
app.use(passport.initialize())
app.use(passport.session())

//Static Folder
app.use(express.static("public"))

//Routes
app.use('/', require('./routes/index'))
app.use('/auth', require('./routes/auth'))
app.use('/stories', require('./routes/stories'))

const PORT = process.env.PORT || 3000

app.listen(PORT, console.log(`server running in ${process.env.NODE_ENV} mode on port ${PORT}`))