const express = require("express")
const router = express.Router()
const { ensureAuth, ensureGuest } = require('../middleware/auth')
const Story = require("../models/Story")

router.get('/', ensureGuest, (req, res) => {
    res.render('auth/login', {
        layout: 'layouts/login'
    })
})

router.get('/dashboard/:id', ensureAuth, async (req, res) => {
    const user = req.user
    try {
        const stories = await Story.find({ user: req.params.id }).populate('user').lean()
        res.render('user/dashboard', {
            name: req.user.firstName,
            id: req.user.id,
            stories,
            user
        })
    } catch (error) {
        console.error(error)
        res.render('error/500')
    }
})


module.exports = router