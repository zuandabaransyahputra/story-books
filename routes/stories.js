const express = require("express")
const Story = require("../models/Story")
const { ensureAuth } = require('../middleware/auth')

const router = express.Router()

router.get('/add', ensureAuth, async (req, res) => {
    const user = req.user
    res.render("stories/add", { layout: 'layouts/user', user })
})

router.get('/', ensureAuth, async (req, res) => {
    const user = req.user
    try {
        const stories = await Story.find({ status: "public" }).populate('user').sort({ createdAt: 'desc' }).lean()
        res.render("stories/index", {
            layout: 'layouts/user',
            stories,
            user
        })
    } catch (error) {
        console.log(error)
        res.render("error/404", {
            user
        })
    }
})

router.post('/', ensureAuth, async (req, res) => {
    const user = req.user
    try {
        req.body.user = req.user.id
        await Story.create(req.body)
        res.redirect(`/dashboard/${user.id}`)
    } catch (error) {
        console.error(error)
        res.render('error/404', {
            user
        })
    }
})

router.get('/:id', ensureAuth, async (req, res) => {
    const user = req.user
    try {
        const data = await Story.find({ _id: req.params.id }).populate('user')
        const story = data[0]
        if (user.id != story.user.id) {
            res.render('stories/show', {
                layout: 'layouts/user',
                story,
                user
            })
        } else {
            res.render('stories/show', {
                story,
                user
            })
        }
    } catch (error) {
        console.error(error)
        res.render('error/404', {
            user
        })
    }
})

router.delete('/:id', async (req, res) => {
    let story
    const user = req.user
    try {
        story = await Story.findById(req.params.id).populate('user')
        await story.remove()
        res.redirect(`/dashboard/${story.user._id}`)
    } catch (error) {
        console.error(error)
        res.render('error/404', {
            user
        })
    }
})

router.get('/:id/edit', ensureAuth, async (req, res) => {
    const user = req.user
    try {
        const story = await Story.findById(req.params.id).populate('user')
        res.render('stories/edit', { layout: 'layouts/user', story, user })
    } catch (error) {
        console.error(error)
        res.render('error/404', { user })
    }
})

router.put('/:id', ensureAuth, async (req, res) => {
    const user = req.user
    try {
        const story = await Story.findById(req.params.id)
        story.title = req.body.title
        story.body = req.body.body
        story.status = req.body.status
        await story.save()
        res.redirect(`/dashboard/${user.id}`)
    } catch (error) {
        console.error(error)
        res.render('error/404', {
            user
        })
    }
})

router.get('/user/:id', ensureAuth, async (req, res) => {
    const user = req.user
    try {
        const stories = await Story.find({ user: req.params.id, status: 'public' }).populate('user').lean()
        res.render('stories/showStoriesByUser', {
            stories,
            user
        })
    } catch (error) {
        console.error(error)
        res.render('error/404', { user })
    }
})

module.exports = router